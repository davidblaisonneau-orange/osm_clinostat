#!/usr/bin/env python3

import atexit
import logging
import configparser
from stepper.stepper import StepperController
from stepper.mqtt import MqttClient


def cleanup(log, motors):
    for name, motor in motors.items():
        log.info('Cleanup {}'.format(name))
        motor.cleanup()


def main():
    """Main block."""
    # Read Config
    config = configparser.ConfigParser()
    config.read('stepper.ini')

    # Set logs
    logging.basicConfig(format='%(asctime)s %(message)s',
                        filename=config['log']['filename'],
                        level=logging.DEBUG)
    log = logging.getLogger('stepper')
    log.info("Welcome to OSM_clinostat stepper controller")

    motor1 = StepperController('motor1',
                               config['motor1']['enable_pin'],
                               config['motor1']['direction_pin'],
                               config['motor1']['pull_pin'])

    motor2 = StepperController('motor2',
                               config['motor2']['enable_pin'],
                               config['motor2']['direction_pin'],
                               config['motor2']['pull_pin'])

    motors = {"motor1": motor1, "motor2": motor2}
    atexit.register(cleanup, log, motors)
    mqttc = MqttClient('localhost', port=1883, motors=motors)


if __name__ == "__main__":
    main()
