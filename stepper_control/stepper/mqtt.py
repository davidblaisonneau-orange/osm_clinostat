import json
import logging
import paho.mqtt.client as mqtt
from pprint import pprint as pp

CLIENT_ID_SUB = "osm_clinostat_stepper_SUB"
CLIENT_ID_PUB = "osm_clinostat_stepper_PUB"


def on_connect_sub(_client, userdata, flag, rc):
    mqttc = userdata['client']
    # mqttc.log('on_connect :: subscribe')
    _client.subscribe(mqttc.listen_topic)


def on_connect_pub(_client, userdata, flag, rc):
    mqttc = userdata['client']
    mqttc.log('on_connect :: publish')
    _client.publish(mqttc.log_topic, "stepper controller connected")


def on_message(_client, userdata, message):
    mqttc = userdata['client']
    if '/' in message.topic:
        motor = message.topic.split('/')[1:]
        mqttc.log("Received '{}' about '{}'".format(message.payload,
                                                    motor[0]))
        mqttc.on_message(motor, message.payload)
    else:
        mqttc.log("Received '{}' with bad topic '{}'".format(message.payload,
                                                             message.topic),
                  "error")


def on_disconnect(_client, userdata, rc):
    mqttc = userdata['client']
    if rc != 0:
        mqttc.log("Unexpected disconnection.")


class MqttClient():

    def __init__(self, server, motors, port=1883):
        self.server = server
        self.port = port
        self.logger = logging.getLogger("stepper")
        self.log_topic = "log"
        self.listen_topic = "stepper/#"
        self.motors = motors
        self.__mqtt_init()

    def on_message(self, motor, message):
        self.log("receive {} for #{}".format(message, motor))
        motor_id = motor[0]
        if motor_id not in self.motors.keys():
            self.log("Message received about unknown motor "
                     "'{}'".format(motor_id),
                     "error")
        else:
            if len(motor) > 1:
                msg = message.decode('ascii')
                action = motor[1]
                if action == 'speed':
                    self.motors[motor_id].run(int(msg))
                elif action == 'increment':
                    self.motors[motor_id].set_frequency_increment(int(msg))
                elif action == 'action':
                    self.__on_message_subaction(motor_id, msg)
                else:
                    self.log("Unknown action '{}'".format(action), "error")
            else:
                try:
                    msg = json.loads(message)
                except json.decoder.JSONDecodeError:
                    self.log("Can not json parse '{}'".format(message),
                             "error")
                    return False
                if not isinstance(msg, dict):
                    self.log("Bad message format '{}'".format(msg), "error")
                elif 'speed' in msg.keys():
                    if not isinstance(msg['speed'], int):
                        self.log("Bad speed format '{}'".format(msg),
                                 "error")
                        return False
                    self.motors[motor_id].run(msg['speed'])
                elif 'increment' in msg.keys():
                    if not isinstance(msg['increment'], int):
                        self.log("Bad increment format '{}'".format(msg),
                                 "error")
                        return False
                    self.motors[motor_id].set_frequency_increment(
                        msg['increment'])
                elif 'action' in msg.keys():
                    self.__on_message_subaction(motor_id, msg['action'])
                else:
                    self.log("Unknown message '{}'".format(msg), "error")
        return True

    def __on_message_subaction(self, motor_id, subaction):
        if subaction == 'start':
            self.motors[motor_id].run()
        elif subaction == 'stop':
            self.motors[motor_id].stop()
        elif subaction == 'increment':
            self.motors[motor_id].speed_change(1)
        elif subaction == 'decrement':
            self.motors[motor_id].speed_change(-1)
        else:
            self.log("Action unknown '{}'".format(subaction), "error")

    def __mqtt_init(self):
        self.userdata = {
            'client': self,
            'listen_topic': self.listen_topic,
            'log_topic': self.log_topic,
            'motors': self.motors,
        }
        self.pub = mqtt.Client(CLIENT_ID_PUB)
        self.pub.on_connect = on_connect_pub
        self.pub.on_disconnect = on_disconnect
        self.pub.user_data_set(self.userdata)
        self.pub.connect(self.server, self.port)

        self.sub = mqtt.Client(CLIENT_ID_SUB)
        self.sub.on_connect = on_connect_sub
        self.sub.on_disconnect = on_disconnect
        self.sub.on_message = on_message
        self.sub.user_data_set(self.userdata)
        self.sub.connect(self.server, self.port)
        self.send2log("Stepper controller ready")
        self.sub.loop_forever()


    def send2log(self, msg):
        self.log('send "{}" to mqtt topic {}'.format(msg, self.log_topic))
        self.pub.publish(self.log_topic, msg)


    def log(self, msg, level='info'):
        """logging interface."""
        msg = 'mqtt >> {}'.format(msg)
        if level == 'debug':
            self.logger.debug(msg)
        elif level == 'info':
            self.logger.info(msg)
        elif level == 'warning':
            self.logger.warning(msg)
        elif level == 'error':
            self.logger.error(msg)
        elif level == 'critical':
            self.logger.critical(msg)
