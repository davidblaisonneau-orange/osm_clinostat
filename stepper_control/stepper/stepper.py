#!/usr/bin/env python3

import logging
import RPi.GPIO as GPIO

class StepperController():
    """
    Stepper controller.

    This class controller a stepper motor using a Microstep driver
    """

    MAX_FREQ = 10000

    def __init__(self, name, enable, direction, pull, micro=1):
        """
        Stepper controller definition.

        :param name: motor name
        :type name: string
        :param enable: enable gpio pin
        :type enable: int
        :param direction: dir gpio pin
        :type direction: int
        :param pull: pull gpio pin
        :type pull: int
        :param micro: micro division config of the driver
        :type enable: int
        """
        self.name = name
        self.enable = int(enable)
        self.direction = int(direction)
        self.pull = int(pull)
        self.micro = micro
        self.pwm = None
        self.frequency = 1
        self.incr = 0
        self.log = logging.getLogger("stepper")
        self.__gpio_config()
        self.__log('initialized')

    def __repr__(self):
        """
        Return a human representation of this class.

        :return: human representation of this class
        :rtype: string
        """
        repr = ("Stepper controller on pins: \n"
                "  Enable: {}\n"
                "  Dir: {}\n"
                "  Pull: {}\n"
                "with microstep config set to"
                " 1/{}\n"
                "actual frequency to"
                "{} Hz"
                "[incr +-{}]".format(self.enable,
                                     self.direction,
                                     self.pull,
                                     self.micro,
                                     self.frequency,
                                     self.incr))
        return repr

    def run(self, freq=1):
        """
        Run the motor.

        :param freq: frequency of steps, in hertz
        :type freq: int
        """
        self.frequency = freq
        self.__change_dir(freq > 0)
        self.pwm.start(0.5)
        self.pwm.ChangeFrequency(abs(freq))
        self.__log('run motor to {}Hz'.format(freq))

    def stop(self):
        """
        Stop the motor.
        """
        self.pwm.stop()
        self.__log('stop motor')

    def set_frequency_increment(self, incr):
        """
        Set frequency step.

        :param incr: incr/decrement frequency
        :type incr: integer
        """
        self.incr = incr
        self.__log('set increment to {}'.format(incr))

    def speed_change(self, dir):
        """
        Inc/Decrement frequency.

        :param dir: increment or decrement speed
        :type dir: 1 or -1
        """
        if self.frequency < self.MAX_FREQ and dir > 0:
            self.frequency += self.incr
        elif self.frequency > -self.MAX_FREQ and dir < 0:
            self.frequency -= self.incr
        self.run(self.frequency)
        self.__log('set frequency to {}Hz'.format(self.frequency))

    def cleanup(self):
        """Release the GPIO pins."""
        GPIO.cleanup([self.enable, self.direction, self.pull])
        self.__log('cleanup GPIOs')

    def __log(self, msg, level='info'):
        """logging interface."""
        msg = '{} >> {}'.format(self.name, msg)
        if self.log:
            if level == 'debug':
                self.log.debug(msg)
            elif level == 'info':
                self.log.info(msg)
            elif level == 'warning':
                self.log.warning(msg)
            elif level == 'error':
                self.log.error(msg)
            elif level == 'critical':
                self.log.critical(msg)
        else:
            print(msg)

    def __change_dir(self, direction):
        """
        Change the direction of the motor.

        :param direction: 0 for default, 1 for reverse
        """
        if direction:
            GPIO.output(self.direction, GPIO.HIGH)
        else:
            GPIO.output(self.direction, GPIO.LOW)
        self.__log('change direction')


    def __gpio_config(self):
        """Configure all GPIOs."""
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.enable, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.direction, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.pull, GPIO.OUT, initial=GPIO.LOW)
        self.pwm = GPIO.PWM(self.pull, 1)
        self.__log('GPIO configured')
