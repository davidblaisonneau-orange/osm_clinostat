#!/usr/bin/env python3

from glob import glob
from os.path import basename
from os.path import splitext

from setuptools import find_packages
from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

version = {}
with open("src/osm_clinostat/version.py") as fp:
    exec(fp.read(), version)

setup(name='osm_clinostat_stepper',
      description='Open Space Maker Clinostat Stepper Library',
      author='David Blaisonneau',
      author_email='david-space@blaisonneau.fr',
      version=version['__version__'],
      long_description=readme(),
      license='Apache 2.0',
      packages=find_packages('.'),
      py_modules=[splitext(basename(path))[0] for path in glob('stepper/*.py')],
      include_package_data=True,
      install_requires=[
        "RPi.GPIO",
        "paho-mqtt",
        "serial"
      ],
      setup_requires=["pytest-runner"],
      tests_require=[
        "pytest",
        "pytest-cov",
      ],
      zip_safe=False)
