port = 9001;
host = window.location.hostname;
topic = "clinostat/#";
// Called after form input is processed
function startConnect() {
    // Generate a random client ID
    clientID = "oms_gyro_web-" + parseInt(Math.random() * 100);

    // Print output for the user in the messages div
    console.log('Connecting to: ' + host + ' on port: ' + port);
    console.log('Using the following client value: ' + clientID);

    // Initialize new Paho client connection
    client = new Paho.MQTT.Client(host, Number(port), clientID);

    // Set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    // Connect the client, if successful, call onConnect function
    client.connect({
        onSuccess: onConnect,
    });
}

// Called when the client connects
function onConnect() {
    // Print output for the user in the messages div
    console.log('Subscribing to: ' + topic);

    // Subscribe to the requested topic
    client.subscribe(topic);
}

// Called when the client loses its connection
function onConnectionLost(responseObject) {
    console.log("onConnectionLost: Connection Lost");
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost: " + responseObject.errorMessage);
    }
}

// Called when a message arrives
function onMessageArrived(message) {
    // console.log("onMessageArrived: Topic: " + message.destinationName + '  | ' + message.payloadString);
    if (message.destinationName == 'clinostat/angle'){
      vars = JSON.parse(message.payloadString)
      navball_x = vars[0]
      navball_z = vars[1]
      navball_y = vars[2]
      console.log(vars)
    }
}

// Called when the disconnection button is pressed
function startDisconnect() {
    client.disconnect();
    console.log('Disconnected');
}

startConnect();
